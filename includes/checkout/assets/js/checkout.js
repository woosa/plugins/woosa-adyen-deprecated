( function($, woosa){

   if ( ! woosa ) {
      return;
   }

   var Ajax = woosa.ajax;
   var Translation = woosa.translation;
   var Prefix = woosa.prefix;
   var Util = woosa.util;

   var processCheckout = {

      /**
       * Initiates this module.
       */
      init: function(){

         //stop here if Adyen lib is not loaded
         if(typeof AdyenCheckout == 'undefined') return;

         this.init_popup();

         this.generate_web_component();
         this.regenerate_web_component();

         this.display_payment_action();

      },


      /**
       * Init popup used for payment actions
       */
      init_popup: function(){

         $('.'+Prefix+'-popup').each(function(){
            let _this = $(this),
               blur = (typeof _this.attr('data-blur') == 'undefined') ? false : true,
               escape = (typeof _this.attr('data-escape') == 'undefined') ? false : true;

            _this.popup({
               autoopen: true,
               blur: blur,
               escape: escape,
               scrolllock: true
            });
         });
      },


      /**
       * Init Adyen checkout.
       *
       * @returns {object}
       */
      AdyenCheckout: function(){

         return new AdyenCheckout({
            paymentMethodsResponse: woosa.api.response_payment_methods,
            clientKey: woosa.api.origin_key,
            locale: woosa.locale,
            environment: woosa.api.environment,
            onChange: function (state, component) {

               if(state.data.paymentMethod.type == 'scheme'){

                  let methodType = 'bcmc' === component.props.type ? 'bcmc' : state.data.paymentMethod.type;

                  processCheckout.setEncryptedCardData(state, component, methodType);

               }else if($.inArray(state.data.paymentMethod.type, ['ideal', 'dotpay', 'molpay_ebanking_fpx_MY', 'molpay_ebanking_TH'] ) != -1){

                  processCheckout.setBankIssuerData(state, component);

               }else if(state.data.paymentMethod.type == 'blik'){

                  processCheckout.setBlikData(state, component);

               }

            },
            onAdditionalDetails: function (state, component) {

               let elem = $(component._node),
                  order_id = elem.attr('data-order_id');

               jQuery.ajax({
                  url   : Ajax.url,
                  method: 'POST',
                  data: {
                     action     : Prefix+'_additional_details',
                     security   : Ajax.nonce,
                     state_data: state.data,
                     order_id   : order_id
                  },
                  success: function(res) {
                     if(res.data.redirect){
                        window.location.href = res.data.redirect;
                     }
                  }
               });

            },
            onError: function(err){
               console.log(err)
            }
         });
      },


      /**
       * Generates the Web Component of the payment method.
       */
      generate_web_component: function(){

         if($('.'+Prefix+'-datepicker').length > 0){
            $('.'+Prefix+'-datepicker').datepicker({
               dateFormat : "dd-mm-yy",
               changeYear: true,
               changeMonth: true,
            })
         }

         this.generateCardForm();
         this.generateNewCardForm();
         this.generateGooglePay();
         this.generateApplePay();
         this.generateBankIssuer();

      },


      /**
       * Re-generates the Web Component of the payment method after WC checkout is updated.
       */
      regenerate_web_component: function(){

         $(document).on('updated_checkout', this, function(e){

            e.data.generate_web_component();

         });

      },


      /**
       * Displays the payment action based on the received action data
       * @since 1.3.0
       */
      display_payment_action: function(){

         var elem = '#'+Prefix+'-payment-action-data';

         if($(elem).length > 0){

            var action = JSON.parse($(elem).attr('data-payment_action'));

            processCheckout.AdyenCheckout().createFromAction(action).mount(elem);
         }
      },



      /**
       * Clear card data from the hidden fileds.
       *
       * @param {string} methodType
       */
       clearCardForm: function(methodType){

         jQuery('#'+Prefix+'-'+methodType+'-card-number').val('');
         jQuery('#'+Prefix+'-'+methodType+'-card-exp-month').val('');
         jQuery('#'+Prefix+'-'+methodType+'-card-exp-year').val('');
         jQuery('#'+Prefix+'-'+methodType+'-card-cvc').val('');
         jQuery('#'+Prefix+'-'+methodType+'-card-holder').val('');
         jQuery('#'+Prefix+'-'+methodType+'-sci').val('');
         jQuery('#'+Prefix+'-'+methodType+'-store-card').val('');

         Util.debugLog('Clear the encrypted card data.');

      },


      /**
       * Fills the encrypted card data in the hidden fields.
       *
       * @param {object} state
       * @param {object} component
       * @param {string} methodType
       */
      setEncryptedCardData: function(state, component = {}, methodType = ''){

         if(state && state.isValid){

            var store_card = state.data.storePaymentMethod ? state.data.storePaymentMethod : '0';

            if(component && component._node){

               jQuery('#'+component._node.id).data('card_state', state);

               Util.debugLog('Saved temporarily the encrypted card data on the element.');

            }

            jQuery('#'+Prefix+'-'+methodType+'-card-number').val(state.data.paymentMethod.encryptedCardNumber);
            jQuery('#'+Prefix+'-'+methodType+'-card-exp-month').val(state.data.paymentMethod.encryptedExpiryMonth);
            jQuery('#'+Prefix+'-'+methodType+'-card-exp-year').val(state.data.paymentMethod.encryptedExpiryYear);
            jQuery('#'+Prefix+'-'+methodType+'-card-cvc').val(state.data.paymentMethod.encryptedSecurityCode);
            jQuery('#'+Prefix+'-'+methodType+'-card-holder').val(state.data.paymentMethod.holderName);
            jQuery('#'+Prefix+'-'+methodType+'-sci').val(state.data.paymentMethod.storedPaymentMethodId);
            jQuery('#'+Prefix+'-'+methodType+'-store-card').val(store_card);

            if(state.data.installments){
               jQuery('#'+Prefix+'-'+methodType+'-card-installments').val(state.data.installments.value);
            }

            Util.debugLog('Set the encrypted card data.');

         }else{

            this.clearCardForm(methodType);
         }
      },


      /**
       * Fills the selected bank issuer in the hidden field.
       *
       * @param {object} state
       * @param {object} component
       */
      setBankIssuerData: function(state, component){

         var value = '';

         if(state && state.isValid){
            value = state.data.paymentMethod.issuer;
         }

         jQuery('#'+Prefix+'_'+state.data.paymentMethod.type+'_issuer').val(value);

      },


      setBlikData: function(state, component){

         var value = '';

         if(state && state.isValid){
            value = state.data.paymentMethod.blikCode;
         }

         jQuery('#'+Prefix+'_'+state.data.paymentMethod.type+'_code').val(value);
      },


      /**
       * Generates credidcard component.
       */
      generateCardForm: function(){

         jQuery('[data-'+Prefix+'-stored-card]').off('click').on('click', function(){

            var _this        = jQuery(this),
               parent        = _this.parent(),
               current       = parent.find('.'+Prefix+'-stored-card__fields'),
               methodType    = _this.attr('data-'+Prefix+'-stored-card-type'),
               formElemId    = _this.attr('data-'+Prefix+'-stored-card'),
               formElem      = jQuery('#'+formElemId),
               formType      = 'scheme' === methodType ? 'card' : methodType,
               cardState     = formElem.data('card_state'),
               methodIndex   = formElemId.replace(/[^0-9\.]/g, ''),
               storedMethods = processCheckout.AdyenCheckout().paymentMethodsResponse.storedPaymentMethods,
               card_installments = jQuery('[data-'+Prefix+'-card-installments]').val(),
               paymentMethodsConfiguration = '';


            if(formElem.length > 0){

               //new card
               if( '' === methodIndex){

                  if( '' == formElem.children(0).html() ){

                     if(Util.isJson(card_installments)){

                        card_installments = JSON.parse(card_installments);

                        if(card_installments.constructor === Array){

                           paymentMethodsConfiguration = {
                              card: {
                                 installmentOptions: {
                                    card: {
                                       values: card_installments,
                                       // Shows regular and revolving as plans shoppers can choose.
                                       // plans: [ 'regular', 'revolving' ]
                                    },
                                 },
                                 // Shows payment amount per installment.
                                 showInstallmentAmounts: true
                              }
                           }
                        }
                     }

                     processCheckout.AdyenCheckout().setOptions({
                        paymentMethodsConfiguration: paymentMethodsConfiguration,
                     }).create(formType, {
                        brands: woosa.api.card_types,
                        enableStoreDetails: woosa.api.store_card,
                        hasHolderName: true,
                        holderNameRequired: true,
                     }).mount('#'+formElemId);

                     Util.debugLog('Initiated the form for using a new card.');

                  }else{

                     processCheckout.setEncryptedCardData(cardState, {}, methodType);
                  }

                  jQuery('#'+Prefix+'-'+methodType+'-is-stored-card').val('no');

               //stored card
               }else{

                  if('' == formElem.html()){

                     processCheckout.AdyenCheckout().create(formType, storedMethods[methodIndex]).mount('#'+formElemId);

                     Util.debugLog('Initiated the form for the existing card.');

                  }else{

                     processCheckout.setEncryptedCardData(cardState, {}, methodType);
                  }

                  jQuery('#'+Prefix+'-'+methodType+'-is-stored-card').val('yes');
               }
            }

            jQuery('.'+Prefix+'-stored-card__fields').closest('.'+Prefix+'-stored-card').not(parent).removeClass('selected');
            jQuery('.'+Prefix+'-stored-card__fields').not(current).slideUp();

            current.slideToggle();
            parent.addClass('selected');
         });
      },


      /**
       * Generates the new card form if no saved card are present
       */
      generateNewCardForm:function () {

          $( document.body ).on( 'updated_checkout', function () {

              var _this        = jQuery('[data-'+Prefix+'-stored-card]'),
                  parent        = _this.parent(),
                  current       = parent.find('.'+Prefix+'-stored-card__fields'),
                  methodType    = _this.attr('data-'+Prefix+'-stored-card-type'),
                  formElemId    = _this.attr('data-'+Prefix+'-stored-card'),
                  formElem      = jQuery('#'+formElemId),
                  formType      = 'scheme' === methodType ? 'card' : methodType,
                  card_installments = jQuery('[data-'+Prefix+'-card-installments]').val(),
                  paymentMethodsConfiguration = '';


              if(formElem.length > 0) {

                  //new card
                  if (!!formElemId && '' === formElemId.replace(/[^0-9\.]/g, '')) {

                      if ('' == formElem.children(0).html()) {

                          if (Util.isJson(card_installments)) {

                              card_installments = JSON.parse(card_installments);

                              if (card_installments.constructor === Array) {

                                  paymentMethodsConfiguration = {
                                      card: {
                                          installmentOptions: {
                                              card: {
                                                  values: card_installments,
                                                  // Shows regular and revolving as plans shoppers can choose.
                                                  // plans: [ 'regular', 'revolving' ]
                                              },
                                          },
                                          // Shows payment amount per installment.
                                          showInstallmentAmounts: true
                                      }
                                  }
                              }
                          }

                          processCheckout.AdyenCheckout().setOptions({
                              paymentMethodsConfiguration: paymentMethodsConfiguration,
                          }).create(formType, {
                              brands: woosa.api.card_types,
                              enableStoreDetails: woosa.api.store_card,
                              hasHolderName: true,
                              holderNameRequired: true,
                          }).mount('#' + formElemId);

                          Util.debugLog('Initiated the form without saved cards.');

                          jQuery('#'+Prefix+'-'+methodType+'-is-stored-card').val('no');

                          jQuery('.'+Prefix+'-stored-card__fields').closest('.'+Prefix+'-stored-card').not(parent).removeClass('selected');
                          jQuery('.'+Prefix+'-stored-card__fields').not(current).slideUp();

                          current.slideToggle();
                          parent.addClass('selected');
                      }
                  }
              }

          } );


      },


      /**
       * Generates GooglePay component.
       */
      generateGooglePay: function(){

         if(jQuery('#woosa_adyen_googlepay_button').length > 0){

            const test_mode = 'yes' !== jQuery('#woosa_adyen_googlepay_testmode').val() && 'yes' !== woosa.api.test_mode ? false : true,
               merchant_id = jQuery('#woosa_adyen_googlepay_merchant_identifier').val();

            var config = {
               gatewayMerchantId: woosa.api.adyen_merchant,
               merchantName: woosa.site_name,
               merchantId: merchant_id
            };

            const googlepay = this.AdyenCheckout().create("paywithgoogle", {
               environment: test_mode ? 'TEST' : 'PRODUCTION',
               amount: {
                  currency: woosa.currency,
                  value: (woosa.cart.total) * 100, //it's in cents
               },
               configuration: config,
               buttonColor: "white",
               onAuthorized: (data) => {

                  jQuery('#'+Prefix+'-googlepay-container .googlepay-description').html(data.paymentMethodData.description).show();
                  jQuery('#woosa_adyen_googlepay_description').val(data.paymentMethodData.description);
                  jQuery('#woosa_adyen_googlepay_token').val(data.paymentMethodData.tokenizationData.token);
               }
            });


            googlepay
               .isAvailable()
               .then(() => {

                  if(jQuery('.adyen-checkout__paywithgoogle').length == 0){
                     googlepay.mount("#woosa_adyen_googlepay_button");
                  }

               })
               .catch(e => {
                  console.log(e);
                  jQuery('.wc_payment_method .payment_method_woosa_adyen_googlepay').remove();
               });
         }
      },


      /**
       * Generates AppleyPay component.
       */
      generateApplePay: function(){

         let total =  (woosa.cart.total) * 100; // it's in cents

         const applepay = this.AdyenCheckout().create("applepay", {
            amount: {
               currency: woosa.currency,
               value: total.toFixed(), // try in a console JS, there a bug : 68.60 * 100 = 6859.999999999999
            },
            countryCode: woosa.cart.country,
            onAuthorized: (callBackSuccess,callBackError,ApplePayPaymentAutorizedEvent) => {
               let token = JSON.stringify(ApplePayPaymentAutorizedEvent.payment.token.paymentData);
               if( ! ApplePayPaymentAutorizedEvent.payment.token.paymentData) callBackError();
               jQuery('#woosa_adyen_applepay_token').val(token);
               callBackSuccess();
               document.dispatchEvent( new CustomEvent('WoosaApplePayPaymentAutorizedEvent', {'detail': {ApplePayPaymentAutorizedEvent : ApplePayPaymentAutorizedEvent} }));
            }
         });

         applepay
            .isAvailable()
            .then(() => {
               applepay.mount("#applepay-container");
            })
            .catch(e => {
               console.log(e)
               jQuery('.payment_method_woosa_adyen_applepay').remove();
            });
      },



      /**
       * Generates bank issuer component.
       */
      generateBankIssuer: function(){

         var items = ['ideal', 'dotpay', 'blik', 'molpay_ebanking_fpx_MY', 'molpay_ebanking_TH'];

         items.map(function(item){
            var elem = '#'+Prefix+'-'+item+'-container',
               component = processCheckout.AdyenCheckout().create(item);

            if($(elem).length > 0){
               component.mount(elem);
            }
         });
      },
   };

   $( document ).ready( function() {
      processCheckout.init();
   });

})( jQuery, adn_util );
