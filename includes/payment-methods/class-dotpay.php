<?php
/**
 * DotPay payment method
 *
 * @author Woosa Team
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class DotPay extends Ideal{


   /**
    * Constructor of this class.
    *
    */
   public function __construct(){

      Abstract_Gateway::__construct();

      $this->has_fields = true;

   }



   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'PL' => [
            'currencies' => ['PLN'],
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Dotpay', 'woosa-adyen');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){
      return $this->show_supported_country();
   }



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'dotpay';
   }



   /**
    * Returns the payment method to be used for recurring payments
    *
    * @return string
    */
   public function recurring_payment_method(){}



   /**
    * Processes the payment.
    *
    * @param int $order_id
    * @return array
    */
   public function process_payment($order_id) {

      Abstract_Gateway::process_payment($order_id);

      $order     = wc_get_order($order_id);
      $reference = $order_id;
      $payload   = $this->build_payment_payload( $order, $reference );

      $response = $this->api->checkout()->send_payment($payload);

      if($response->status == 200){

         return $this->process_payment_result( $response, $order );

      }else{

         wc_add_notice($response->body->message, 'error');
      }

      return ['result' => 'failure'];

   }


}